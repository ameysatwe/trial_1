FROM node:10

RUN npm init -y && npm install --save typescript @types/node 
WORKDIR /usr/src/app # Only copy the package.json file to work directory
COPY package.json .
ADD . /usr/src/app
# Install all Packages
RUN npm install #Copy all other source code to work directory
ADD . /usr/src/app
# TypeScript
RUN npm run tsc